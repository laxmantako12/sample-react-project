import { timeStamp } from "console";
import mongoose, { Mongoose } from "mongoose";
const userSchema = mongoose.Schema({
    username: {
        type: String,
        required:[true, "Must provide a user name."],
        unique: [true, "Must be unique."]
    },
    email: {
        type: String,
        required: [true, "Must provide an email."],
        unique: [true, "Must be unique."]
    },
    password: {
        type: String,
        required: [true, "Must provide a password."],
    },

},
{
  timestamps: true
});

const User = Mongoose.models.user || mongoose.model("User", userSchema);
export default User;
