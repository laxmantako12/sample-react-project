import { BriefcaseBusiness, School } from "lucide-react";
// Assuming LucideProps is defined by Lucide package
// import { LucideProps } from "lucide-react";

// interface TimelineData {
//   date: string;
//   title: string;
//   subtitle: string;
//   description: string;
//   icon: React.ComponentType<LucideProps>; // Specify the icon type correctly
// }
const timelineDatas = [
  {
    date: "2011 - present",
    title: "Creative Director",
    subtitle: "Miami, FL",
    description:
      "Creative Direction, User Experience, Visual Design, Project Management, Team Leading",
    icon: BriefcaseBusiness,
  },
  {
    date: "2010 - 2011",
    title: "Art Director",
    subtitle: "San Francisco, CA",
    description:
      "Creative Direction, User Experience, Visual Design, SEO, Online Marketing",
    icon: BriefcaseBusiness,
  },
  {
    date: "2008 - 2010",
    title: "Web Designer",
    subtitle: "Los Angeles, CA",
    description: "User Experience, Visual Design",
    icon: BriefcaseBusiness,
  },
  {
    date: "2006 - 2008",
    title: "Web Designer",
    subtitle: "San Francisco, CA",
    description: "User Experience, Visual Design",
    icon: BriefcaseBusiness,
  },
  {
    date: "April 2013",
    title: "Content Marketing for Web, Mobile and Social Media",
    subtitle: "Online Course",
    description: "Strategy, Social Media",
    icon: School,
  },
  {
    date: "November 2012",
    title: "Agile Development Scrum Master",
    subtitle: "Certification",
    description: "Creative Direction, User Experience, Visual Design",
    icon: School,
  },
  {
    date: "2002 - 2006",
    title: "Bachelor of Science in Interactive Digital Media Visual Imaging",
    subtitle: "Bachelor Degree",
    description: "Creative Direction, Visual Design",
    icon: School,
  },
  // Additional timeline elements can be added here
];

export default timelineDatas;
