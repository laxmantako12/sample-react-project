"use client";
import React, { useState, useEffect } from "react";
import { Star } from "lucide-react";
import timelineDatas from "./components/timelineData"; // Correct import statement
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";

import "react-vertical-timeline-component/style.min.css";

const AboutPage = () => {
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const handleScroll = () => {
      const timelineElement = document.querySelector(".about");
      if (!timelineElement) return;

      const timelinePosition = timelineElement.getBoundingClientRect().top;
      const windowHeight = window.innerHeight;

      if (timelinePosition < windowHeight / 1) {
        setIsVisible(true);
      } else {
        setIsVisible(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div className="about bg-grey-dark/5 py-12 lg:py-[80px] min-h-[300px]">
      <div className="container">
        <h2 className="text-lg">AboutPage</h2>
        <VerticalTimeline>
          {timelineDatas.map((timelineData, index) => (
            <VerticalTimelineElement
              key={index}
              className={`vertical-timeline-element--work ${
                isVisible ? "is-timeline-visible" : "is-timeline-hidden"
              }`}
              date={timelineData.date}
              iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
            //   icon={timelineData.icon}
            >
              <h3 className="vertical-timeline-element-title">
                {timelineData.title}
              </h3>
              <h4 className="vertical-timeline-element-subtitle">
                {timelineData.subtitle}
              </h4>
              <p>{timelineData.description}</p>
            </VerticalTimelineElement>
          ))}
          <VerticalTimelineElement
            iconStyle={{ background: "rgb(16, 204, 82)", color: "#fff" }}
            icon={<Star />}
          />
        </VerticalTimeline>
      </div>
    </div>
  );
};

export default AboutPage;
