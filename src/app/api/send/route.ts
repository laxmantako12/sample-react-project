import { EmailTemplate } from '../../../components/EmailTemplate';
import { Resend } from 'resend';
import { NextRequest, NextResponse } from 'next/server';

const resend = new Resend(process.env.NEXT_PUBLIC_RESEND_API);


export async function POST(req:NextRequest): Promise<NextResponse> {
    console.log('req', req.body);
    try {
      const response: any = await resend.emails.send({
        from: 'Acme <onboarding@resend.dev>',
        to: ['laxmantako12@gmail.com'],
        subject: 'Hello world',
        react: EmailTemplate({ firstName: 'John' }),
        text: 'Hello world, this is a plain text version of the email.',
      });
  
      if (response.error) {
        return NextResponse.json({ error: response.error }, { status: 500 });
      }
  
      return NextResponse.json(response.data);
    } catch (error) {
      return NextResponse.json({ error: (error as Error).message }, { status: 500 });
    }
  }
