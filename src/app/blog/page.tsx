"use client";
import { useState, useEffect } from "react";
import { LoaderCircle } from 'lucide-react';
import Search from "@/components/search";

interface Post {
  id: number;
  title: string;
  body: string;
}

const Blog = () => {
  const [data, setData] = useState<Post[]>([]); // Initialize with an empty array
  const [isLoading, setLoading] = useState(true);
  const [visibleCount, setVisibleCount] = useState(20);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts/")
      .then((response) => response.json())
      .then((data: Post[]) => { // Specify the type for the fetched data
        setData(data);
        setLoading(false);
      });
  }, []);

  const loadMore = () => {
    setVisibleCount((prevCount) => prevCount + 20); // Increment by 20
  };

  if (isLoading) return <p className="text-center py-10"><LoaderCircle className="animate-spin ml-auto mr-auto"  size={36}/>Loading...</p>;
  if (data.length === 0) return <p className="text-center py-10">No blog data</p>;

  return (
    <div className="blog bg-grey-dark/5 py-12 lg:py-[80px]">
      <div className="container">
        {/* <Search /> */}
        <div className="lg:flex lg:flex-wrap -mx-3 gap-y-10">
          {data.slice(0, visibleCount).map((post) => (
            <div className="lg:basis-1/4 px-3" key={post.id}> {/* Move key here */}
              <div className="bg-white hover:bg-white-foreground/30 shadow cursor-pointer p-8 transition-all hover:shadow-xl">
                <span className="font-bold">{post.id}</span>
                <h1 className="font-bold">{post.title}</h1>
                <p>{post.body}</p>
              </div>
            </div>
          ))}
          {visibleCount < data.length && ( // Show load more button if there are more items to load
            <button onClick={loadMore}>Load More</button>
          )}
        </div>
      </div>
    </div>
  );
};

export default Blog;
