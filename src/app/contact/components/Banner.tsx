import React from 'react'
import bannerImg from '../../../../public/images/TW_Contact_Us.png';
import Image from "next/image";

const bgImg = {
  backgroundImage: `url(${bannerImg.src})`,
  backgroundSize: "contain",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  minHeight: "365px",
  backgroundColor: "#eceff1",
};
const bgColor = {
  backgroundColor: "#eceff1",
};
const Banner = () => {
  return (
    // <div className="banner py-8 bg-primary/5 text-xl" style={bgImg}>
    <div className="banner py-8  text-xl text-center" style={bgColor}>
      <div className="container">
        <Image className='inline' src={bannerImg} alt="Contact Image" priority />
      </div>
    </div>
  );
}

export default Banner
