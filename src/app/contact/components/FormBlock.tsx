import React from "react";
import FormContent from "./FormContent";
import ContactForm from "./ContactForm";

const FormBlock = () => {
  return (
    <div className="form-block bg-white py-20">
      <div className="container">
        <div className="lg:flex lg:flex-wrap -mx-4">
          <div className="px-4 lg:basis-1/2">
            <FormContent />
          </div>
          <div className="px-4 lg:basis-1/2">
            <ContactForm />
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormBlock;
