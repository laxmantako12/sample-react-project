import React from "react";
import Link from "next/link";
import SocialMedia from "./SocialMedia";
const FormContent = () => {
  return (
    <div className="lg:w-2/3">
      <h2 className="text-[30px] mb-[15px] text-slate-500 font-bold">
        Hey, great to see you here!
      </h2>
      <p className="mb-4">
        We’re all about turning visions into reality. Go ahead and fill out the
        form for a game-changing free Strategy Session with me, the director.
        Trust me, you won’t want to miss it.
      </p>
      <p>
        Already got a project brief? Perfect! Send it directly to me at
        <Link
          href="mailto:laxmantako12@gmail.com"
          className="text-secondary inline-block"
        >
          laxmantako12@gmail.com
        </Link>
        .
      </p>
      {/* <SocialMedia className="mt-4" /> */}

      <SocialMedia />
    </div>
  );
};

export default FormContent;
