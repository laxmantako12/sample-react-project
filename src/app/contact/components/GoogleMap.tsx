import React from "react";

const GoogleMap = () => {
  return (
    <div className="map">
      <iframe
        className="w-full grayscale border-0 outline-none"
        src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d56529.7766024344!2d85.34207894951986!3d27.68296281341843!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1719413291294!5m2!1sen!2snp"
        height="450"
        style={{ border: 0 }}
        allowFullScreen
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      ></iframe>
    </div>
  );
};

export default GoogleMap;
