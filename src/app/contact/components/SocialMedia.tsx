import React from "react";
import Link from "next/link";
import { Facebook, Linkedin, Instagram } from "lucide-react";
const SocialMedia = () => {
  return (
    <div className="social-media mt-8">
      <h3 className="text-lg font-bold mb-[10px]">Follow us on</h3>
      <ul className="flex gap-4">
        <li>
          <Link href="/">
            <Facebook />
          </Link>
        </li>
        <li>
          <Link href="/">
            <Linkedin />
          </Link>
        </li>
        <li>
          <Link href="/">
            <Instagram />
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default SocialMedia;
