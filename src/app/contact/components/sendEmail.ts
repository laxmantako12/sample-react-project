'use server'
import { Resend } from 'resend';

export const sendEmail = async () => {
  const resend = new Resend(process.env.RESEND_API_KEY);

  const { data } = await resend.emails.send({
    from: 'Acme <onboarding@resend.dev>',
    to: ['laxmantako12@gmail.com'],
    subject: 'Hello World',
    html: '<strong>It works!</strong>'
  });

  return data;
};
