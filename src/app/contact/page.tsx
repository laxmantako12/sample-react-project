
import React from 'react'
import Banner from './components/Banner';
import GoogleMap from './components/GoogleMap';
import FormBlock from './components/FormBlock';

const Contact = () => {
  return (
    <>
    <Banner />
    <FormBlock />
    <GoogleMap />

    </>
  );
}

export default Contact
