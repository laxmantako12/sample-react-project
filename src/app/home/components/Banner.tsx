
import React from "react";
import { Hexagon } from "lucide-react";
import SocialMedia from "@/components/SocialMedia";
import Link from "next/link";
import Image from "next/image";
import Author from "../../../../public/images/professional-smiling-woman.jpg";
// import Author from '../../../../public/images/laxman.png';
import { ProfessionalSmiling } from "@/assets/images";


const Banner = () => {
  return (
    <div className="mainBanner bg-grey-dark py-12 lg:py-[80px]">
      <div className="container">
        <div className="lg:flex items-center lg:flex-wrap -mx-3 text-white tracking-[0.075rem]">
          <div className="lg:basis-1/2 px-3">
            <span className="text-[24px] font-semibold">Hello, Its Me</span>
            {/* <h1 className="text-[40px] font-extrabold mb-[5px]">Laxman Tako</h1> */}
            <h1 className="text-[40px] font-extrabold mb-[5px]">
              Your Name ...
            </h1>
            <h2 className="text-[24px] font-bold mb-[20px]">
              And me a
              <span className="text-green-shine"> Frontend Developer</span>
            </h2>
            <p className="text-[16px] font-sm tracking-[0.025rem] mb-5 w-[75%]">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum
              corporis nihil laborum a magnam.
            </p>
            <SocialMedia />
            <Link
              href=""
              className="mt-4 bg-green-shine inline-block rounded-[30px] m-w-[150px] text-[14px] tracking-[.15em] text-grey-dark font-bold py-[10px] px-[30px] "
            >
              Download CV
            </Link>
          </div>
          <div className="lg:basis-1/2 px-3">
            <div className="text-center">
              <Image
                className="mask mask-hexagon-2 inline-block z-10"
                src={ProfessionalSmiling}
                alt=" author"
                priority
              />

              {/* <Hexagon
                className="blur-[0px] ml-auto mr-auto"
                stroke-width="1"
                size={230}
                stroke="#14e7ee"
                fill="#14e7ee"
              /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
