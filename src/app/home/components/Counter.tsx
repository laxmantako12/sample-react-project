"use client";
import React, { useState } from "react";
import { Button } from "@/components/ui/button";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { ToastAction } from "@/components/ui/toast";
import { useToast } from "@/components/ui/use-toast";

const Counter = () => {
  const { toast } = useToast();
  const [count, setCount] = useState(0);
  function decrease() {
    if (count <= 0) {
      // alert("limit negative");
      toast({
        variant: "destructive",
        title: "Uh oh! Something went wrong.",
        description: "There was a problem with your request.",
        action: <ToastAction altText="Try again">Try again</ToastAction>,
      });
    } else {
      setCount(count - 1);
    }
  }
  function increase() {
    if (count >= 5) {
      //alert("limit reached");
      toast({
        title: "Uh oh! Your limit reached.",
        description: "Maximum product order limit reached.",
      });
    } else {
      setCount(count + 1);
    }
  }
  function reset() {
    setCount(0);
  }
  return (
    <div className="py-12 bg-white text-xl">
      <div className="container">
        <div className="Counter">
          <Card className="shadow max-w-[360px] bg-white text-center">
            <CardHeader>
              <CardTitle>Counter</CardTitle>
              <p>{count}</p>
              <Button
                className="bg-secondary hover:bg-secondary-foreground text-white"
                onClick={reset}
              >
                Reset
              </Button>
            </CardHeader>
            <CardContent>
              <div className="flex gap-2 justify-center">
                <Button
                  className="bg-primary hover:bg-secondary text-white"
                  onClick={decrease}
                >
                  -
                </Button>
                <Button
                  className="bg-primary hover:bg-secondary text-white"
                  onClick={increase}
                >
                  +
                </Button>
              </div>
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Counter;
