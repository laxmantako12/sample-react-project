import React from 'react'
import Link from 'next/link';
import bannerImg from '../../../../public/images/Web-Design.png';

const bgImg = {
  backgroundImage: `url(${bannerImg.src})`,
  backgroundSize: "cover",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  minHeight: "465px",
  backgroundColor: "#eceff1",
};
const Quote = () => {
  return (
    <div className='quote py-12 lg:py-[80px] relative z-[1] flex items-center' style={bgImg}>
        <div className="bg-black/70 absolute w-full h-full top-0 left-0 z-[-1]"></div>
        <div className="container">
            <div className="text-center">
            <span className="block text-sm uppercase font-bold tracking-[.15em] text-secondary mb-4">Your best choice</span>
            <h2 className="text-[36px] text-white font-bold mb-8 leading-[100%]">Lets start your <br /> new dream project</h2>
            <Link className='inline-block rounded-[30px] m-w-[150px] uppercase bg-secondary text-[14px] tracking-[.15em] text-white font-bold py-[10px] px-[30px] hover:bg-white transition hover:text-secondary' href="">Get a quote</Link>
            </div>
        </div>
    </div>
  )
}



export default Quote
