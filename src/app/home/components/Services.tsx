
import React from "react";
import serviceData from "./services/serviceData";
const Services = () => {
  return (
    <div className="services py-12 lg:py-[80px]">
      <div className="container">
        <div>
          <div className="lg:flex  lg:flex-wrap -mx-3">
            {/* Map over the sliderData array to render each services */}
            {serviceData.map((services, index) => (
              <div key={index} className="lg:basis-1/4 px-3">
                <div className="bg-white hover:bg-white-foreground/30 shadow cursor-pointer p-8 text-center transition-all hover:shadow-xl">
                  <span className="mb-4 mr-auto ml-auto leading-[80px] bg-primary/5 flex items-center justify-end text-[120px] h-[80px] w-[80px] rounded-[50%] text-stroke font-black">
                    <services.icon
                      size="40"
                      strokeWidth={1}
                      stroke="#121217"
                      className="mr-auto ml-auto"
                    />{" "}
                    {/* Adjust size if needed */}
                  </span>

                  <h3 className="text-[18px] font-bold mb-6 leading-[120%]">
                    {services.title}
                  </h3>
                  <p className="leading-[140%] text-[15px] font-normal mb-4">
                    {services.description}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>

    </div>
  );
};

export default Services;
