import React from "react";
import StepsContent from "./steps/StepsContent";
import StepsSlide from "./steps/StepsSlide";

const Steps = () => {
  return (
    <div className="py-12 lg:py-[80px] bg-primary/5 text-xl step">
      <div className="container">
        <div className="lg:flex lg:flex-wrap -mx-3">
          <div className="lg:basis-1/4 px-3">
            <StepsContent />
          </div>
          <div className="lg:basis-9/12 px-3">
            <StepsSlide />

          </div>
        </div>
      </div>
    </div>
  );
};

export default Steps;
