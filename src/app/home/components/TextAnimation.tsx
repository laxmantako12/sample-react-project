
"use client";
import React from "react";
import TextReveal from "text-reveal";
const TextAnimation = () => {
  return (
    <>
      <div className="py-12 lg:py-[80px] bg-black">
        <div className="container">
          <div
            style={{
              fontSize: "3rem",
              fontWeight: "800",
            }}
          >
            <TextReveal
              text={[
                "Hello",
                "world",
                "We will measure your kitchen,",
                "ask you questions and start",
                "the actual design process with you",
              ]}
              textColor="white"
              fillColor="linear-gradient(90deg, #12C2E9 0%, #c471ed 50%, #f64f59 100%)"
              fillSpeed={120}
              // fillDirection="top-bottom"
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default TextAnimation
