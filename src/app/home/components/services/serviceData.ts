import { Lightbulb, Receipt, Handshake, Aperture   } from 'lucide-react';
const serviceData = [
    {
      icon: Lightbulb,
      title: "Full Service",
      description:
        "We will measure your kitchen, ask you questions and start the actual design process with you."
    },
    {
        icon: Receipt,
      title: "Deliver Value",
      description:
        "We will measure your kitchen, ask you questions and start the actual design process with you."
    },
    {
        icon: Handshake,
      title: "Partners",
      description:
        "We will measure your kitchen, ask you questions and start the actual design process with you."
    },
    {
        icon: Aperture,
        title: "Integrity",

        description:
          "We will measure your kitchen, ask you questions and start the actual design process with you."
      }
  ];
  
  export default serviceData;
  