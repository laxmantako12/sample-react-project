import React from 'react'

const StepsContent = () => {
  return (
    <div>
      <span className='block text-sm uppercase font-bold tracking-[.15em] text-secondary mb-2'>HOW WE WORKS</span>
      <h2 className='text-[36px] font-bold mb-6 leading-[120%]'> 3 Easy steps to get premium design</h2>
      <p className='text-[18px] leading-[140%]'>The of your kitchen varies from one layout to another. The shape of the kitchen also determines the and space for cabinets, countertops, and accessories.</p>
    </div>
  )
}

export default StepsContent
