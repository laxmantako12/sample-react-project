// StepsSlide.tsx

import React from "react";
import sliderData, { Step } from "./sliderData";

const StepsSlide = () => {
  return (
    <>
      <div className="lg:flex lg:flex-wrap -mx-3">
        {/* Map over the sliderData array to render each step */}
        {sliderData.map((step: Step, index: number) => (
          <div key={index} className="lg:basis-1/3 px-3">
            <div className="bg-white shadow p-8">
              <span className="mb-4 block text-[120px] text-stroke font-black leading-[100%]">
                {step.number}
              </span>
              <h3 className="text-[18px] font-bold leading-[120%]">
                {step.title}
              </h3>
              <h3 className="text-[18px] font-bold mb-6 leading-[120%]">
                {step.subtitle}
              </h3>
              <p className="leading-[140%] text-[15px] font-normal mb-4">
                {step.description}
              </p>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default StepsSlide;
