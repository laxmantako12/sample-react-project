// sliderData.ts

export interface Step {
    number: string;
    title: string;
    subtitle: string;
    description: string;
  }
  
  const sliderData: Step[] = [
    {
      number: "01",
      title: "We Visit",
      subtitle: " You At Home",
      description:
        "We will measure your kitchen, ask you questions and start the actual design process with you."
    },
    {
      number: "02",
      title: "We Visit",
        subtitle: " You At Home",
      description:
        "We will measure your kitchen, ask you questions and start the actual design process with you."
    },
    {
      number: "03",
      title: "We Visit",
        subtitle: " You At Home",
      description:
        "We will measure your kitchen, ask you questions and start the actual design process with you."
    }
  ];
  
  export default sliderData;
  