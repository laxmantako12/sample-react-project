import React from "react";
import Counter from "./components/Counter";
import Steps from "./components/Steps";
import Services from "./components/Services";
import Quote from "./components/Quote";
import Banner from "./components/Banner";
import Testimonial from "@/components/Testimonial";
import TextAnimation from "./components/TextAnimation";

const HomePage = () => {
  return (
    <>

        <Banner />
        {/* <Counter /> */}
        <Services />
        <TextAnimation />
        <Quote />
        <Steps />
        <Testimonial />

    </>
  );
};

export default HomePage;
