import Test from "@/components/Test";
import { redirect } from 'next/navigation'
import FrontPage from "./home/page";
export default function Home() {
  // Logic to determine if a redirect is needed
  // const accessDenied = true
  // if (accessDenied) {
  //   redirect('/login')
  // }
 
  // Define other routes and logic
  return (
    <>
      <FrontPage />
      {/* <Test /> */}
    </>
  );
}
