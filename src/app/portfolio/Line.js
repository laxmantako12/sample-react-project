import React from "react";

const Line = ({ color, topPosition }) => {
    const lineStyle = {
        backgroundColor: color,
    };

    const dotStyle = {
        backgroundColor: color,
    };

    return (
        <div className="line bg-grey-light/20 dark:bg-white/40 w-[1px] min-h-full relative" style={{ height: '100%' }}>
            <div className="absolute left-[-3px]" style={{ top: `${topPosition}%`, transform: 'translateY(-50%)' }}>
                <span className="animate-ping absolute inline-flex h-[8px] w-[8px] rounded-full opacity-75" style={dotStyle}></span>
                <span className="inline-flex absolute rounded-full h-[8px] w-[8px]" style={dotStyle}></span>
            </div>
        </div>
    );
};

export default Line;
