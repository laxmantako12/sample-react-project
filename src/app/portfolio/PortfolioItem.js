import React from "react";
import Image from "next/image";
import Link from "next/link";


const PortfolioItem = ({ title, image, link, index, className }) => {
    const isEven = index % 2 === 1;
    return (
        <div className={`lg:basis-1/2 px-3 ${isEven ? 'lg:mt-[150px]' : ''}`}>
            <Link href={link} target="_blank" className="block">
            <h3 className="uppercase font-bold text-[11px] mb-[10px] tracking-[0.075rem] text-slate-500 dark:text-white">
                {index + 1}. {title}
            </h3>
            <div className="bg-white p-[4px] rounded-lg border border-slate-100 shadow-lg h-[300px] relative z-10">
                <Image
                        className="rounded-lg w-full h-[300px] object-cover object-top"
                    src={image}
                    width={400}
                    height={400}
                    alt={title}
                />
            </div>
            </Link>
        </div>
    );
};

export default PortfolioItem;
