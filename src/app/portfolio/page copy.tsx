import React from "react";
import Image from "next/image";
import portfolio1 from "../../../public/images/professional-smiling-woman.jpg"
import portfolio2 from "../../../public/images/testimonial.jpg";
import portfolio3 from "../../../public/images/Web-Design.png";
import portfolio4 from "../../../public/images/nature-banner-background-6.jpg";
import portfolio5 from "../../../public/images/photography.jpg";
import portfolio6 from "../../../public/images/sunset.jpg";

const portfolio = () => {
  return (
    <>
      <div className="portfolio relative py-[80px] overflow-hidden bg-slate-50 dark:bg-primary">
        <div className="lines flex justify-around h-full fixed left-0 top-0 w-full">
          <div className="line bg-grey-light/20 dark:bg-white/40 w-[1px] min-h-full relative">
            <div className="absolute left-[-3px] top-[35%] z-10">
              <span className="animate-ping absolute inline-flex h-[8px] w-[8px] rounded-full bg-sky-500 opacity-75"></span>
              <span className="inline-flex absolute rounded-full h-[8px] w-[8px] bg-sky-500"></span>
            </div>
          </div>
          <div className="line bg-grey-light/20 dark:bg-white/40 w-[1px] min-h-full relative">
            <div className="absolute left-[-3px] top-[55%] z-10">
              <span className="animate-ping absolute inline-flex h-[8px] w-[8px] rounded-full bg-secondary opacity-75"></span>
              <span className="inline-flex absolute rounded-full h-[8px] w-[8px] bg-secondary"></span>
            </div>
          </div>
          <div className="line bg-grey-light/20 dark:bg-white/40 w-[1px] min-h-full relative">
            <div className="absolute left-[-3px] top-[15%] z-10">
              <span className="animate-ping absolute inline-flex h-[8px] w-[8px] rounded-full bg-green-shine opacity-75"></span>
              <span className="inline-flex absolute rounded-full h-[8px] w-[8px] bg-green-shine"></span>
            </div>
          </div>
          <div className="line bg-grey-light/20 dark:bg-white/40 w-[1px] min-h-full relative">
            <div className="absolute left-[-3px] top-[75%] z-10">
              <span className="animate-ping absolute inline-flex h-[8px] w-[8px] rounded-full bg-slate-400 opacity-75"></span>
              <span className="inline-flex absolute rounded-full h-[8px] w-[8px] bg-slate-400"></span>
            </div>
          </div>
          <div className="line bg-grey-light/20 dark:bg-white/40 w-[1px] min-h-full relative">
            <div className="absolute left-[-3px] top-[30%] z-10">
              <span className="animate-ping absolute inline-flex h-[8px] w-[8px] rounded-full bg-secondary opacity-75"></span>
              <span className="inline-flex absolute rounded-full h-[8px] w-[8px] bg-secondary"></span>
            </div>
          </div>
          <div className="line bg-grey-light/20 dark:bg-white/40 w-[1px] min-h-full relative">
            <div className="absolute left-[-3px] top-[55%] z-10">
              <span className="animate-ping absolute inline-flex h-[8px] w-[8px] rounded-full bg-green-shine opacity-75"></span>
              <span className="inline-flex absolute rounded-full h-[8px] w-[8px] bg-green-shine"></span>
            </div>
          </div>
        </div>
        <div className="container  ">
          <div className="lg:flex lg:flex-wrap -mx-3 gap-y-10">
            <div className="lg:basis-1/2 px-3">
              <h3 className="uppercase font-bold text-[11px] mb-[10px] tracking-[0.075rem] text-slate-500 dark:text-white">
                01. MY project title
              </h3>
              <div className="bg-white p-[4px] rounded-lg border border-slate-100 shadow min-h-[300px] relative z-10">
                <Image
                  className="rounded-lg w-full h-[300px] object-cover"
                  src={portfolio1}
                  width={500}
                  height={500}
                  alt="Picture of the author"
                />
              </div>
            </div>

            <div className="lg:basis-1/2 px-3 lg:mt-[150px]">
              <h3 className="uppercase font-bold text-[11px] mb-[10px] tracking-[0.075rem] text-slate-500 dark:text-white">
                02. MY project title 2
              </h3>
              <div className="bg-white p-[4px] rounded-lg border border-slate-100 shadow min-h-[300px] relative z-10">
                <Image
                  className="rounded-lg w-full h-[300px] object-cover"
                  src={portfolio2}
                  width={500}
                  height={500}
                  alt="Picture of the author"
                />
              </div>
            </div>
            <div className="lg:basis-1/2 px-3">
              <h3 className="uppercase font-bold text-[11px] mb-[10px] tracking-[0.075rem] text-slate-500 dark:text-white">
                03. MY project title 3
              </h3>
              <div className="bg-white p-[4px] rounded-lg border border-slate-100 shadow min-h-[300px] relative z-10">
                <Image
                  className="rounded-lg w-full h-[300px] object-cover"
                  src={portfolio3}
                  width={500}
                  height={500}
                  alt="Picture of the author"
                />
              </div>
            </div>
            <div className="lg:basis-1/2 px-3 lg:mt-[150px]">
              <h3 className="uppercase font-bold text-[11px] mb-[10px] tracking-[0.075rem] text-slate-500 dark:text-white">
                04. MY project title 4
              </h3>
              <div className="bg-white p-[4px] rounded-lg border border-slate-100 shadow min-h-[300px] relative z-10">
                <Image
                  className="rounded-lg w-full h-[300px] object-cover"
                  src={portfolio4}
                  width={500}
                  height={500}
                  alt="Picture of the author"
                />
              </div>
            </div>

            <div className="lg:basis-1/2 px-3">
              <h3 className="uppercase font-bold text-[11px] mb-[10px] tracking-[0.075rem] text-slate-500 dark:text-white">
                03. MY project title 3
              </h3>
              <div className="bg-white p-[4px] rounded-lg border border-slate-100 shadow min-h-[300px] relative z-10">
                <Image
                  className="rounded-lg w-full h-[300px] object-cover"
                  src={portfolio5}
                  width={500}
                  height={500}
                  alt="Picture of the author"
                />
              </div>
            </div>
            <div className="lg:basis-1/2 px-3 lg:mt-[150px]">
              <h3 className="uppercase font-bold text-[11px] mb-[10px] tracking-[0.075rem] text-slate-500 dark:text-white">
                04. MY project title 4
              </h3>
              <div className="bg-white p-[4px] rounded-lg border border-slate-100 shadow min-h-[300px] relative z-10">
                <Image
                  className="rounded-lg w-full h-[300px] object-cover"
                  src={portfolio6}
                  width={500}
                  height={500}
                  alt="Picture of the author"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default portfolio;
