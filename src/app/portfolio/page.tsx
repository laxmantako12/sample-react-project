import React from "react";
import PortfolioItem from "./PortfolioItem";
import Line from "./Line";
import portfolio1 from "../../../public/images/professional-smiling-woman.jpg";
import portfolio2 from "../../../public/images/testimonial.jpg";
import portfolio3 from "../../../public/images/Web-Design.png";
import portfolio4 from "../../../public/images/nature-banner-background-6.jpg";
import portfolio5 from "../../../public/images/photography.jpg";
import portfolio6 from "../../../public/images/sunset.jpg";
import {
  Stone1,
  Stone2,
  Stone3,
  BinaryElectron,
  PatlekhetEcoFarmhouse,
} from "@/assets/images";

const portfolio = () => {
  interface DataColors {
    [key: string]: string;
    bgSky500: string;
    bgSecondary: string;
    bgGreenShine: string;
    bgSlate400: string;
    bgPrimary: string;
  }

  const dataColors: DataColors = {
    bgSky500: "#0ea5e9",
    bgSecondary: "#d6a24c",
    bgGreenShine: "#14e7ee",
    bgSlate400: "#94a3b8",
    bgPrimary: "#1a237e",
  };
  const linesData = [
    { color: "bgSky500", topPosition: 35 },
    { color: "bgSecondary", topPosition: 55 },
    { color: "bgGreenShine", topPosition: 15 },
    { color: "bgSlate400", topPosition: 75 },
    { color: "bgSecondary", topPosition: 30 },
    { color: "bgGreenShine", topPosition: 55 },
  ];

  const portfolioData = [
    {
      title: "Binary Electron",
      image: BinaryElectron,
      link: "https://binaryelectron.com/",
    },
    {
      title: "Patlekhet Eco Farmhouse",
      image: PatlekhetEcoFarmhouse,
      link: "https://patlekhetecofarm.com.np/",
    },
    { title: "MY project title", image: Stone1, link: "#" },
    { title: "MY project title", image: Stone2, link: "" },
    { title: "MY project title", image: Stone3, link: "" },
    { title: "MY project title", image: portfolio1, link: "" },
    { title: "MY project title", image: portfolio2, link: "" },
    // { title: "MY project title", image: portfolio3 },
    { title: "MY project title", image: portfolio4, link: "" },
    { title: "MY project title", image: portfolio5, link: "" },
    { title: "MY project title", image: portfolio6, link: "" },
  ];
  return (
    <>
      <div className="portfolio relative py-[80px] overflow-hidden bg-slate-50 dark:bg-primary">
        <div className="lines flex justify-around h-full fixed left-0 top-0 w-full">
          {linesData.map((line, index) => (
            <Line
              key={index} // Ensure each item has a unique key
              color={dataColors[line.color]}
              topPosition={line.topPosition}
            />
          ))}
        </div>
        <div className="container">
          <div className="lg:flex lg:flex-wrap -mx-3 gap-y-10">
            {portfolioData.map((item, index) => (
              <PortfolioItem
                className=""
                link={item.link}
                key={index} // Ensure each item has a unique key
                title={item.title}
                image={item.image}
                index={index}
              />
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default portfolio;
