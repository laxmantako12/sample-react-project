"use client";
import React from "react";
import { Input } from "@/components/ui/input";

const SearchApiForm = ({ searchText, setSearchtext }) => {
  return (
    <div className="bg-primary p-10 min-h-[300px] flex items-center">
      <form
        className="max-w-[500px] w-full ml-auto mr-auto space-y-6"
        onSubmit={(e) => {
          e.preventDefault();
        }}
        action="#"
      >
        <Input
          placeholder="Search jobs"
          value={searchText}
          onChange={(e) => setSearchtext(e.target.value)}
          //   onChange={(e) => console.log(e.target.value)}
        />
        {/* <Button type="submit">Submit</Button> */}
      </form>
    </div>
  );
};

export default SearchApiForm;
