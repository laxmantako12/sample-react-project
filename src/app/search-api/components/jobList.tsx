import Link from "next/link";
import { Bookmark } from "lucide-react";

const JobList = ({ jobItems }) => {
  if (!jobItems) return null; // Render nothing if jobItems is undefined

  return (
    <div>
      {jobItems.map((jobItem) => (
        <Link key={jobItem.id} href={`/jobs/${jobItem.id}`} className="flex items-start gap-2 justify-between">
          <div className="job-item__badge bg-gray-500 rounded h-[30px] w-[30px] text-sm leading-[30px] text-white text-center">
            {jobItem.badgeLetters}
          </div>
          <div className="job-item__middle w-[200px]">
            <h3 className="job-item__heading font-medium -mt-[5px]">
              {jobItem.title}
            </h3>
            <p className="job-item__company text-xs font-medium italic">
              {jobItem.company}
            </p>
            <p className="job-item__salary text-sm font-medium text-red-600">
              {jobItem.location}
            </p>
          </div>
          <div className="job-item__right leading-0 text-right">
            <Bookmark fill={"#475569"} stroke={"475569"} className="mr-0 ml-auto" />
            <time className="job-item__time text-sm mr-1">
              {jobItem.daysAgo} days
            </time>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default JobList;
