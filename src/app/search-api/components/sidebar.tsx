import React from "react";
import Link from "next/link";
import { Bookmark } from "lucide-react";

interface SidebarProps {
  id?: number;
  title?: string;
  company?: string;
  salary?: number | string;
  daysAgo?: number;
  badgeLetters?: string;
  location?: string;
}

const Sidebar: React.FC<{ sidebar: SidebarProps }> = ({ sidebar }) => {
  return (
      <Link href="" className="flex items-start gap-2 justify-between">
        <div className="job-item__badge bg-gray-500 rounded h-[30px] w-[30px] text-sm leading-[30px] text-white text-center">
          {sidebar.badgeLetters}
        </div>
        <div className="job-item__middle w-[200px]">
          <h3 className="job-item__heading font-medium -mt-[5px]">
            {sidebar.title}
          </h3>
          <p className="job-item__company text-xs font-medium italic">
            {sidebar.company}
          </p>
          <p className="job-item__salary text-sm font-medium text-red-600 ">
            {sidebar.location}
          </p>
        </div>
        <div className="job-item__right leading-0 text-right">
          <Bookmark
            fill={"#475569"}
            stroke={"475569"}
            className="mr-0 ml-auto"
          />
          <time className="job-item__time text-sm mr-1">
            {sidebar.daysAgo} days
          </time>
        </div>
      </Link>

  );
};

export default Sidebar;
