"use client";
import { useState, useEffect } from "react";
import Sidebar from "./components/sidebar";
import SearchApiForm from "./components/SearchApiForm";
import JobListItem from "./components/JobListItem";

const SearchForm = () => {
  const [searchText, setSearchtext] = useState("");
  const [jobItems, setjobItems] = useState([]);
  const [error, setError] = useState<string | null>(null);
  //console.log(jobItems);

  useEffect(() => {
    //console.log(searchText);
    if (!searchText) return;

    const fetchData = async () => {
      try {
        const response = await fetch(
          `https://bytegrad.com/course-assets/projects/rmtdev/api/data?search=${searchText}`
        );
        if (!response.ok) {
          throw new Error("Network response was not ok.");
        }
        const data = await response.json();
        setjobItems(data.jobItems);
        console.log(data.jobItems);
        setError(null); // Clear any previous error
      } catch (error) {
        console.error("Error fetching data:", error);
        setError("Error fetching data. Please try again later."); // Set error state with string message
      }
    };

    fetchData();
  }, [searchText]);

  return (
    <div className=" pt-0 ">
      <SearchApiForm searchText={searchText} setSearchtext={setSearchtext} />

      <JobListItem  />

      <div className="container py-10">
        {jobItems.length === 0 ? (
          <p>No jobs found.</p>
        ) : (
          jobItems.map((jobItem) => (
            <>
              <Sidebar sidebar={jobItem} />

            </>

          ))
        )}
      </div>
    </div>
  );
};

export default SearchForm;
