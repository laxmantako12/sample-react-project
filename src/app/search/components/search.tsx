
"use client"
import React, { useState } from 'react'
import { Input } from "@/components/ui/input"


const Search = (props: any) => {
  const [searchVal, setSearchVal] = useState('');
  const handleInput = (e:any) => {
    setSearchVal(e.target.value);
  }

  const handleClearBtn = () => {
    setSearchVal('');
  }

  const filteredProducts = props.products.filter((product: any) => {
    return product.includes(searchVal);
  });
  return (
    <div className='container py-10'>
      <div className='input-wrap'>
        <i className="fas fa-search"></i>

        <Input
          onChange={handleInput}
          value={searchVal}
          type="text"
          name="product-search"
          id="product-search"
          placeholder="Search Products"
        />
        <div
          onClick={handleClearBtn}
          className="fas fa-times"
        >Clear</div>
      </div>
      <div className="results-wrap">
        <ul className='flex flex-wrap gap-5'>
          {filteredProducts.map((product: any) => {
            return <li key={product} className='inline-block'><a href='#'>{product}</a></li>
          })}
        </ul>
      </div>
    </div>
  )
}

export default Search
