import React from 'react'
import Search from './components/search';

const searchPage = () => {
    let products = [
        'apples', 'bananas', 'grapefruit', 'kiwi', 'avocados', 
        'lettuce', 'tomatoes', 'cheese', 'bread', 'yogurt', 
        'peas', 'carrots', 'broccoli', 'beans', 'pizza',
        'pasta', 'rice', 'cereal', 'butter', 'milk',
        'eggs', 'onions', 'garlic', 'honey', 'soup',
        'salt', 'pepper', 'oregano', 'basil', 'paprika'
      ];
  return (
    <Search products={products} />
  )
}

export default searchPage