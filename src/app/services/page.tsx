import TextStyle from '@/components/TextStyle';
import React from 'react'

const Services = () => {
  return (
    <div className="py-8 bg-primary/5 text-2xl">
      <div className="container">This is Services page.
        <TextStyle />
      </div>
    </div>
  );
}

export default Services
