import React from 'react'

const Shop = () => {
  return (
    <div className="py-8 bg-primary/5 text-2xl">
      <div className="container">This is Shop page.</div>
    </div>
  );
}

export default Shop
