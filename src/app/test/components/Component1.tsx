
"use client"
import React from 'react'
import { useState, createContext, useContext } from 'react'

const UserContext = createContext("");
const Component = () => {
    const [user, setUser] = useState("Jesse Hall");
  return (
    <div className='container py-10'>
        <UserContext.Provider value={user}>
      <h1 className='text-[24px] font-bold mb-[20px]'>{`Hello ${user}!`}</h1>
      {/* <Component2 /> */}
    </UserContext.Provider>
    </div>
  )
}

export default Component