"use client"
import React from 'react';
import { createContext, useContext, useState } from 'react';
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { Sun, Moon } from 'lucide-react';


const ThemeContext = createContext('light');
const Component2 = () => {
    const [theme, setTheme] = useState('light');
    const toggleTheme = () => {
        setTheme(prevTheme => (prevTheme === 'light' ? 'dark' : 'light'));
    };
  return (
    <>
    <div>Component2</div>
    <ThemeContext.Provider value={theme} >
    <div className={`flex flex-wrap p-10 w-full max-w-sm items-center space-y-2 dark: ${theme === 'dark' ? 'bg-gray-800 text-white ' : 'bg-primary-extralight text-black'}`}>
    <div className='basis-full'>Component2</div>     
      <Input type="email" placeholder="Email" />
      <Button type="submit">Subscribe</Button>
    </div>
      </ThemeContext.Provider>
      <Button onClick={toggleTheme}>
      {theme === 'light' ? <Sun size={24} /> : <Moon size={24} />}
      </Button>
    </>
  )
}

export default Component2