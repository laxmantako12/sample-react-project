
import Component1 from "./components/Component1"
import Component2 from "./components/Component2"
import Parallax from "./components/Parallax"


const Test = () => {
   
  return (
    <>
    <Component1 />
    <Component2 />
    <Parallax />
    </>
  )
}

export default Test