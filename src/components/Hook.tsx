"use client";
import React, { useState } from "react";
import { Button } from "@/components/ui/button";

const Hook = () => {
    const [count, setCount] = useState(0);
  const [color, setColor] = useState("red");
  const [car, setCar] = useState({
    brand: "Ford",
    model: "Mustang",
    year: "1964",
    color: "red",
  });
   const updateColor = () => {
     setCar((previousState) => {
       return { ...previousState, color: "blue" };
     });
   };
  return (
    <>
      <main className="flex min-h-screen flex-col p-24">
        <div className="container">
          <p className="my-2">You clicked {count} times</p>
          <button onClick={() => setCount(count + 1)}>Click me</button>
          <Button
            className="bg-secondary text-white"
            onClick={() => setCount(count + 1)}
          >
            Click me
          </Button>
          <hr className="my-10" />
          <h1>
            My favorite color is <strong style={{ color }}>{color}</strong>!
          </h1>
          <Button
            className="bg-secondary text-white mr-1"
            type="button"
            onClick={() => setColor("blue")}
          >
            Blue
          </Button>
          <Button
            className="bg-secondary text-white mr-1"
            type="button"
            onClick={() => setColor("red")}
          >
            Red
          </Button>
          <Button
            className="bg-secondary text-white mr-1"
            type="button"
            onClick={() => setColor("pink")}
          >
            Pink
          </Button>
          <Button
            className="bg-secondary text-white mr-1"
            type="button"
            onClick={() => setColor("green")}
          >
            Green
          </Button>
          <hr className="my-10" />
          <h1>My {car.brand}</h1>
          <p>
            It is a <strong style={{ color: car.color }}>{car.color}</strong>{" "}
            {car.model} from {car.year}.
          </p>
          <Button
            className="bg-secondary text-white mr-1"
            type="button"
            onClick={updateColor}
          >
            Red
          </Button>
        </div>
      </main>
    </>
  );
}

export default Hook
