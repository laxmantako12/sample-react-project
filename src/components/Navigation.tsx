"use client";
import React, { useEffect, useState } from "react";
import { Dialog, DialogPanel } from "@headlessui/react";
// import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import { Menu, X, Search, Sun, Moon } from "lucide-react";
import { usePathname } from "next/navigation";
import Link from "next/link";
import Mode from "./Mode";
import { FLD, CreativeLogo } from "@/assets/images";
import Image from "next/image";

const navigation = [
  { name: "Home", href: "/" },
  // { name: "Shop", href: "/shop" },
  { name: "About", href: "/about" },
  { name: "Services", href: "/services" },
  { name: "Blog", href: "/blog" },
  { name: "Portfolio", href: "/portfolio" },
  { name: "Contact", href: "/contact" },
];
// Define colors
const colors = {
  activeLink: "#3b82f6",
  inactiveLink: "#6b7280",
};
const Navigation = () => {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const [isScrolled, setIsScrolled] = useState(false);
  const pathname = usePathname();
  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      if (scrollPosition > 150) {
        setIsScrolled(true);
      } else {
        setIsScrolled(false);
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);
  return (
    <div className="bg-white">
      <header
        className={`header relative shadow bg-white inset-x-0 top-0 z-50 ${
          isScrolled ? "scrolled" : ""
        }`}
      >
        <div className="container">
          <nav
            className="flex items-center justify-between py-4"
            aria-label="Global"
          >
            <div className="flex lg:flex-1">
              <a href="#" className="-m-1.5 p-1.5">
                <span className="sr-only">Your Company</span>
                {/* <img
                  className="h-8 w-auto"
                  src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                  alt=""
                /> */}
                {/* <Image  priority

                  className="h-8 w-auto"
                  src={FLD}
                  width={500}
                  height={500}
                  alt="Logo"
                /> */}
                <Image
                  priority
                  className="h-8 w-auto"
                  src={CreativeLogo}
                  width={500}
                  height={500}
                  alt="Logo"
                />
              </a>
            </div>
            <div className="flex lg:hidden">
              <button
                type="button"
                className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
                onClick={() => setMobileMenuOpen(true)}
              >
                <span className="sr-only">Open main menu</span>
                <Menu className="h-6 w-6" aria-hidden="true" />
              </button>
            </div>
            <div className="hidden lg:flex lg:gap-x-12">
              {navigation.map((item) => (
                // <Link
                //   key={item.name}
                //   href={item.href}
                // style={{
                //       color:
                //         pathname === item.href
                //           ? colors.activeLink
                //           : colors.inactiveLink,
                //     }}
                //   className={`text-sm font-semibold leading-6 text-gray-900 link ${
                //     pathname === "/" ? "active" : ""
                //   }`}
                // >
                //   {item.name}
                // </Link>
                <Link
                  key={item.name}
                  href={item.href}
                  // style={{
                  //       color:
                  //         pathname === item.href
                  //           ? colors.activeLink
                  //           : colors.inactiveLink,
                  //     }}
                  className={`link no-underline ${
                    pathname === item.href ? "active" : ""
                  } text-[11px] tracking-[0.15rem] uppercase font-bold text-primary hover:text-secondary transition [&.active]:text-secondary`}
                >
                  {item.name}
                </Link>
              ))}

              {/*   {navigation.map((item) => (
                <Link
                  key={item.name}
                  href={`/${item.name.toLowerCase()}`}
                  className={`text-sm font-semibold leading-6 text-gray-900 ${
                    pathname === `/${item.name.toLowerCase()}` ? "active" : ""
                  }`}
                >
                  {item.name}
                </Link>
              ))} */}
            </div>
            <div className="hidden lg:flex lg:flex-1 lg:justify-end items-center gap-4">
              {/* <Sun /> */}
              <Mode />
              <Link href={"search-api"}>
                <Search size={18}/>
              </Link>
            </div>
          </nav>
        </div>
        <Dialog
          className="lg:hidden"
          open={mobileMenuOpen}
          onClose={setMobileMenuOpen}
        >
          <div className="fixed inset-0 z-50" />
          <DialogPanel className="fixed inset-y-0 right-0 z-50 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
            <div className="flex items-center justify-between">
              <a href="#" className="-m-1.5 p-1.5">
                <span className="sr-only">Your Company</span>
                <Image
                  priority
                  className="h-8 w-auto"
                  src={CreativeLogo}
                  width={500}
                  height={500}
                  alt="Logo"
                />
              </a>
              <button
                type="button"
                className="-m-2.5 rounded-md p-2.5 text-gray-700"
                onClick={() => setMobileMenuOpen(false)}
              >
                <span className="sr-only">Close menu</span>
                <X className="h-6 w-6" aria-hidden="true" />
              </button>
            </div>
            <div className="mt-6 flow-root">
              <div className="-my-6 divide-y divide-gray-500/10">
                <div className="space-y-2 py-6">
                  {navigation.map((item) => (
                    <a
                      key={item.name}
                      href={item.href}
                      className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                    >
                      {item.name}
                    </a>
                  ))}
                </div>
                <div className="py-6 flex gap-4">
                  {/* <Sun /> */}
                  <Mode />
                  <Search />
                </div>
              </div>
            </div>
          </DialogPanel>
        </Dialog>
      </header>
    </div>
  );
};

export default Navigation;
