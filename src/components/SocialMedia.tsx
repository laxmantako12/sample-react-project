import React from "react";
import Link from "next/link";
import { Facebook, Linkedin, Instagram } from "lucide-react";
const SocialMedia = () => {
  return (
    <div className="social-media">
      <ul className="flex gap-4">
        <li>
          <Link href="/">
            <Facebook />
          </Link>
        </li>
        <li>
          <Link href="/">
            <Linkedin />
          </Link>
        </li>
        <li>
          <Link href="/">
            <Instagram />
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default SocialMedia;
