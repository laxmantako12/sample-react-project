"use client"
import React, { useEffect, useState } from "react";
import { Button } from "@/components/ui/button"


const Test = () => {
  const [count, setCount] = useState(0);
  const [color, setColor] = useState("red");
  const [calculation, setCalculation] = useState(0);
  useEffect(() => {
    setCalculation(() => count * 2);
  }, [count]);


  return (
    <div className="container">
      <div className="py-10">
        <p>You clicked {count} times</p>
        {/* <button onClick={() => setCount(count + 1)}>Click me</button> */}
        <Button
          onClick={() => setCount(count + 1)}
          className="bg-primary text-white"
        >
          Click me
        </Button>
      </div>
      <hr />
      <h1 className="mt-3">
        My favorite color is <span style={{ color }}>{color}</span>!
      </h1>
      <div className="flex gap-2 mt-2">
        <Button
          onClick={() => setColor("blue")}
          className="bg-primary text-white"
        >
          Blue
        </Button>
        <Button
          onClick={() => setColor("red")}
          className="bg-primary text-white"
        >
          Red{" "}
        </Button>
        <Button
          onClick={() => setColor("green")}
          className="bg-primary text-white"
        >
          Green
        </Button>
        <Button
          onClick={() => setColor("purple")}
          className="bg-primary text-white"
        >
          Purple
        </Button>
      </div>
      <div className="flex flex-col gap-2 mt-2">
        <p>Count: {count}</p>
        <Button
          className="bg-primary text-white w-10"
           onClick={() => setCount((a) => a + 1)}
        >
          +
        </Button>
        <p>Calculation: {calculation}</p>
      </div>
    </div>
  );
};

export default Test;
