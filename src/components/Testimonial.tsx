import React from "react";
import clientImg from "../../public/images/testimonial.jpg";
import clientBG from "../../public/images/h10-background04.png";
import testimonialData from '../components/testimonialData'
import Image from 'next/image'
const bgImg = {
  backgroundImage: `url(${clientImg.src})`,
  backgroundSize: "cover",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  minHeight: "465px",
//   backgroundColor: "#eceff1",
};


const Testimonial = () => {

  return (
    <div className="testimonial py-12 lg:py-[80px] relative">
      <div
        className="testimonial-bg hidden lg:block absolute w-[50%] right-0 top-0 h-[100%]"
        style={bgImg}
      ></div>
      {/* <div
        className=" absolute w-[50%] left-0"
        style={bgImg2}
      ></div> */}
      <div className="container">
        <div className="relative z-10">
          <span className="block text-sm uppercase font-bold tracking-[.15em] text-secondary mb-4">
            Our Clients
          </span>
          <h2 className="text-[36px] font-bold mb-8 leading-[100%]">
            Here&apos;s what our satisfied <br /> clients are saying
          </h2>
        </div>

        <div className="lg:flex  lg:flex-wrap -mx-3 relative z-10">
          {/* Map over the sliderData array to render each testimonials */}
          {testimonialData.map((testimonials, index) => (
            <div key={index} className="lg:basis-1/3 px-3">
              <div className="bg-white hover:bg-white-foreground/90 shadow cursor-pointer p-8 text-center transition-all hover:shadow-xl">
                <span className="mb-4 mr-auto ml-auto leading-[80px] bg-primary/5 flex items-center justify-end text-[120px] h-[80px] w-[80px] rounded-[50%] text-stroke font-black">
                  <Image
                    className="h-[80px] w-[80px] rounded-[50%] object-cover"
                    src={testimonials.clientImg}
                    alt="Picture of the author"
                    width={500}
                    height={500}
                  />
                  {/* <img src{testimonials.clientImg} alt="" /> */}
                  <testimonials.icon
                    size="40"
                    strokeWidth={1}
                    stroke="#121217"
                    className="mr-auto ml-auto"
                  />
                  {/* Adjust size if needed */}
                </span>
                <h3 className="text-[18px] font-bold mb-2 leading-[120%]">
                  {testimonials.name}
                </h3>
                <h4 className="text-[15px] text-secondary font-bold mb-6 leading-[120%]">
                  {testimonials.desination}
                </h4>

                <p className="leading-[140%] text-[15px] font-normal mb-4">
                  {testimonials.description}
                </p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Testimonial;
