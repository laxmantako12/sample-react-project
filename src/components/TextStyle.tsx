"use client";
import React from "react";
import AnimatedText from "animated-text-letters";
import "animated-text-letters/index.css";

const TextStyle = () => {
  return (
    <div>
      <AnimatedText
        text="Animated Text Demo"
        animation="slide-left"
        delay={32}
        easing="ease"
        transitionOnlyDifferentLetters={true}
        animationDuration={1000}
      />
    </div>
  );
};

export default TextStyle;


