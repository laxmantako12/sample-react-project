import { Quote } from "lucide-react";
import clientImg from "../../public/images/Web-Design.png";
import clientImg2 from "../../public/images/Web-Design.png";
import clientImg3 from "../../public/images/professional-smiling-woman.jpg";
import clientImg4 from "../../public/images/Web-Design.png";
const testimonialData = [
  {
    clientImg: clientImg,
    icon: Quote,
    name: "Full Service",
    desination: "Designer",
    description:
      "We will measure your kitchen, ask you questions and start the actual design process with you.",
  },
  {
    clientImg: clientImg2,
    icon: Quote,
    name: "Deliver Value",
    desination: "Designer",
    description:
      "We will measure your kitchen, ask you questions and start the actual design process with you.",
  },
  {
    clientImg: clientImg3,
    icon: Quote,
    name: "Partners",
    desination: "Designer",
    description:
      "We will measure your kitchen, ask you questions and start the actual design process with you.",
  },
  // {
  //   clientImg: "../../public/images/testimonial.jpg",
  //   icon: Quote,
  //   name: "Integrity",
  //   desination: "Designer",
  //   description:
  //     "We will measure your kitchen, ask you questions and start the actual design process with you.",
  // },
];

export default testimonialData;
