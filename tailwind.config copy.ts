// const { fontFamily } = require("tailwindcss/defaultTheme");
import type { Config } from "tailwindcss";
const {
  default: flattenColorPalette,
} = require("tailwindcss/lib/util/flattenColorPalette");
const config = {
  darkMode: ["class"],
  content: [
    "./pages/**/*.{ts,tsx}",
    "./components/**/*.{ts,tsx}",
    "./app/**/*.{ts,tsx}",
    "./src/**/*.{ts,tsx}",
  ],
  prefix: "",
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1300px",
      },
    },
    extend: {
      // fontFamily: {
      //   sans: ["var(--font-oswald)", ...fontFamily.oswald],
      // },
      colors: {
        border: "#ecf0f1",
        input: "#34495e",
        ring: "#3498db",
        background: "#ffffff",
        foreground: "#2c3e50",
        primary: {
          DEFAULT: "#2c3e50",
          foreground: "#ffffff",
          normal: "#0232DE",
          light: "#F9CCC3",
          darker: "#B0442F",
          medium: "#4567F3",
          ultralight: "#A3B4F9",
          extralight: "#EEF1FF",
        },
        secondary: {
          DEFAULT: "#d6a24c",
          foreground: "hsl(var(--secondary-foreground))",
          light: "#F9E1B3",
          ultralight: "#E7E9EA",
        },
        white: {
          DEFAULT: "#FFF",
          foreground: "#f1f2f6",
          cityLight: "#dfe4ea",
          twinkleWhite: "#ced6e0",
        },
        grey: {
          normal: "#414145",
          light: "#707073",
          dark: "#20242d",
        },

        green: {
          light: "#B3BABD",
          dark: "#081820",
          shine: "#14e7ee",
        },

        destructive: {
          DEFAULT: "#e74c3c",
          foreground: "#fff",
        },
        muted: {
          DEFAULT: "#e67e22",
          foreground: "#707073",
        },
        accent: {
          DEFAULT: "#f1c40f",
          foreground: "#f39c12",
        },
        popover: {
          DEFAULT: "#3498db",
          foreground: "#2980b9",
        },
        table: {
          normal: "#07131A",
        },

        card: {
          DEFAULT: "#ecf0f1",
          foreground: "#bdc3c7",
        },
      },
      borderRadius: {
        lg: "var(--radius)",
        md: "calc(var(--radius) - 2px)",
        sm: "calc(var(--radius) - 4px)",
      },
      keyframes: {
        "accordion-down": {
          from: { height: "0" },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: "0" },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
      },
    },
  },
  plugins: [
    require('tailwindcss-animate'),
    require('daisyui'),
    addVariablesForColors
  ],
} satisfies Config;

export default config;

// This plugin adds each Tailwind color as a global CSS variable, e.g. var(--gray-200).
function addVariablesForColors({ addBase, theme }: any) {
  let allColors = flattenColorPalette(theme("colors"));
  let newVars = Object.fromEntries(
    Object.entries(allColors).map(([key, val]) => [`--${key}`, val])
  );
 
  addBase({
    ":root": newVars,
  });
}
